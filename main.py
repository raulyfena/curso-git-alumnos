from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    # Comentarios útiles
    return "Hola todo el mundo!"

def suma(a,b):
    return a+b
    
def resta(a,b):
    return a-b
    
if __name__ == "__main__":
    # Corremos la aplicacion
    app.run()